# Teltonika FM-XXXX TCP Encoder/decoder

Документация протокола 
https://wiki.teltonika.lt/view/Codec

Реализация для Codec 8
https://wiki.teltonika.lt/view/Codec_8

Устройство
https://wiki.teltonika.lt/view/FMB120

Расшифровка по IO элементам https://wiki.teltonika.lt/view/FMB_AVL_ID

Ссылки на быблиотеки, код которых использовался
https://github.com/uro/teltonika-fm-parser

https://github.com/cf-git/teltonika-functions/blob/master/teltonika.php