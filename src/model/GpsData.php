<?php

namespace customsoft\teltonika\model;

use function base_convert;
use function bindec;
use function str_pad;
use function substr;
use const STR_PAD_LEFT;

class GpsData implements \JsonSerializable
{
    /**
     * @var float
     */
    private $longitude;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var int
     */
    private $altitude;

    /**
     * @var int
     */
    private $angle;

    /**
     * @var int
     */
    private $satellites;

    /**
     * @var int
     */
    private $speed;

    public function __construct(
        float $longitude,
        float $latitude,
        int $altitude,
        int $angle,
        int $satellites,
        int $speed
    )
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->altitude = $altitude;
        $this->angle = $angle;
        $this->satellites = $satellites;
        $this->speed = $speed;
    }

    public static function decode(string $payload, int $startPosition):array
    {
        $position = $startPosition;

        $longitude = self::determineCoordinate(substr($payload, $position, 8));
        $position += 8;

        $latitude = self::determineCoordinate(substr($payload, $position, 8));
        $position += 8;

        $altitude = hexdec(substr($payload, $position, 4));
        $position += 4;

        $angle = hexdec(substr($payload, $position, 4));
        $position += 4;

        $satellites = hexdec(substr($payload, $position, 2));
        $position += 2;

        $speed = hexdec(substr($payload, $position, 4));
        $position += 4;

        return [
            'data' => new self($longitude, $latitude, $altitude, $angle, $satellites, $speed),
            'length' => $position - $startPosition,
        ];
    }

	public static function determineCoordinate(string $hexCoordinate): float
	{
		$bin = str_pad(base_convert($hexCoordinate, 16, 2), 32, 0, STR_PAD_LEFT);

		if($bin{0} === '1') {
			for ( $i = 0; $i < 32; $i ++ ) {
				switch ( $bin{$i} ) {
					case '0' :
						$bin{$i} = '1';
						break;
					case '1' :
						$bin{$i} = '0';
						break;
				}
			}

			return -1 * bindec($bin)/10000000;
		}

		return bindec($bin)/10000000;
	}

    public function jsonSerialize(): array
    {
        return [
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'altitude' => $this->altitude,
            'angle' => $this->angle,
            'satellites' => $this->satellites,
            'speed' => $this->speed
        ];
    }
}
